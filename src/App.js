import './App.css';
import {Form, Navbar, Button, Nav, FormControl} from 'react-bootstrap';
import React from 'react';
import Home from './component/Home'
import Jerman from "./component/Jerman"
import Turki from "./component/Turki"
import Australia from "./component/Australia"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  return (
    <>
    <Navbar bg="light" expand="lg">
  <Navbar.Brand href="/">Home</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/turki">Turkey</Nav.Link>
      <Nav.Link href="/jerman">Germany</Nav.Link>
      <Nav.Link href="/australia">Australia</Nav.Link>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-success">Search</Button>
    </Form>
  </Navbar.Collapse>
</Navbar>

    <Router>
          <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/turki">
            <Turki />
          </Route>
          <Route path="/jerman">
            <Jerman />
            </Route>
            <Route exact path="/australia">
              <Australia />
            </Route>
            </Switch>
          </Router>

    </>
  );
}

export default App;
